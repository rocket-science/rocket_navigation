# source: https://github.com/pdf/simple-navigation-bootstrap/blob/master/lib/simple_navigation/rendering/renderer/bootstrap.rb
# Copyright (c) 2017 Peter Fern
# MIT License

module RocketNavigation
  module Renderer
    class Bootstrap < RocketNavigation::Renderer::Base
      def initialize(container, options = {})
        super(container, options)
      end

      def list_tag
        options[:ordered] ? :ol : :ul
      end

      def render(item_container)
        if skip_if_empty? && item_container.empty?
          ''.html_safe
        else
          if item_container.level > 1
            content = list_content(item_container)
            if options[:superfish]
              content_tag(list_tag, content, {class: "dropdown-menu"})
            else
              content_tag(:div, content, {class: "dropdown-menu"})
            end
          else
            content = list_content(item_container)
            content_tag(list_tag, content, container_html)
          end
        end
      end

      def render_item(item)
        if item.level == 1
          li_content = tag_for(item)
          if include_sub_navigation?(item)
            li_content << render_sub_navigation_for(item)
          end
          content_tag(:li, li_content, item_options(item))
        else
          if include_sub_navigation?(item)
            content = tag_for(item)
            content << render_sub_navigation_for(item)
            if options[:superfish]
              content_tag(:li, content, class: "dropdown")
            else
              content_tag(:div, content, class: "dropdown")
            end
          else
            tag_for(item)
          end
        end
      end

      def list_content(item_container)
        ret = ActiveSupport::SafeBuffer.new
        item_container.items.each do |item|
          ret << render_item(item)
        end
        ret
      end

      def expand_all?
        true
      end

      def consider_sub_navigation?(item)
        return false unless item.sub_navigation
        true
      end

      def item_extra_classes(item)
        if include_sub_navigation?(item)
          ["dropdown"]
        else
          []
        end
      end

      def link_classes(item)
        if item.level > 1
          classes = ["dropdown-item"]
          if item.selected?
            classes.push('active')
          end
          classes
        else
          super(item)
        end
      end

      def tag_for(item)
        if item.sub_navigation
          cl = ""
          if item.level == 1
            cl = "nav-link"
          else
            cl = "dropdown-item"
          end
          opt = {
            class: cl
          }
          opt[:class] += " dropdown-toggle"
          unless options[:superfish]
            opt['data-toggle'] = "dropdown"
          end
          link_to(item.name, item.url, opt)
        else
          super(item)
        end
      end
    end
  end
end
